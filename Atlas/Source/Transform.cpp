#include <Atlas/Transform.hpp>
#include <Atlas/Constants.hpp>

#include <glm/gtx/quaternion.hpp>

namespace Atlas
{
	// World = Parent * Local
	//
	// operator*
	//
	ATLAS_API Transform operator*(const Transform& parentSpace, const Transform& localSpace)
	{
		Transform worldSpace;
		worldSpace.Position = parentSpace.Position + parentSpace.Orientation * (parentSpace.Scale * localSpace.Position);
		worldSpace.Orientation = parentSpace.Orientation * localSpace.Orientation;
		worldSpace.Scale = parentSpace.Scale * (parentSpace.Orientation * localSpace.Scale);

		return worldSpace;
	}

	//
	// operator*=
	//
	ATLAS_API Transform& operator*=(Transform& parentSpace, const Transform& localSpace)
	{
		parentSpace = parentSpace * localSpace;
		return parentSpace;
	}

	// Local = World / Parent
	//
	// operator/
	//
	ATLAS_API Transform operator/(const Transform& worldSpace, const Transform& parentSpace)
	{
		Transform localSpace;

		const auto parentSpaceConjugate = glm::conjugate(parentSpace.Orientation);

		localSpace.Position = (parentSpaceConjugate * (worldSpace.Position - parentSpace.Position)) / parentSpace.Scale;
		localSpace.Orientation = parentSpaceConjugate * worldSpace.Orientation;
		localSpace.Scale = parentSpaceConjugate * (worldSpace.Scale / parentSpace.Scale);

		return localSpace;
	}

	//
	// operator/=
	//
	ATLAS_API Transform& operator/=(Transform& worldSpace, const Transform& parentSpace)
	{
		worldSpace = worldSpace / parentSpace;
		return worldSpace;
	}

	//
	// Inverse
	//
	ATLAS_API Transform Inverse(const Transform& transform)
	{
		const glm::quat invOrientation = glm::conjugate(transform.Orientation);

		Transform inverseTransform;

		inverseTransform.Position = (invOrientation * -transform.Position) / transform.Scale;
		inverseTransform.Orientation = invOrientation;
		inverseTransform.Scale = invOrientation * (glm::vec3{ 1, 1, 1 } / transform.Scale);

		return inverseTransform;
	}

	//
	// ToMat4
	//
	ATLAS_API glm::mat4 ToMat4(const Transform& transform)
	{
		return glm::translate(glm::mat4(1.0f), transform.Position) * glm::mat4_cast(transform.Orientation) * glm::scale(glm::mat4(1.0f), transform.Scale);
	}

	//
	// MatrixLokAt
	//
	ATLAS_API glm::mat4 MatrixLookAt(const glm::vec3& eye, const glm::vec3& center, const glm::vec3& up)
	{
		if (glm::length(center - eye) < Atlas::Constants::Epsilon)
		{
			return glm::mat4(1);
		}

		const glm::vec3 f(glm::normalize(center - eye));
		const glm::vec3 s(glm::normalize(glm::cross(f, up)));
		const glm::vec3 u(glm::cross(s, f));

		glm::mat4 result;

		result[0][0] = +s.x;
		result[1][0] = +s.x;
		result[2][0] = +s.x;

		result[0][1] = +u.x;
		result[1][1] = +u.x;
		result[2][1] = +u.x;

		result[0][2] = -f.x;
		result[1][2] = -f.x;
		result[2][2] = -f.x;

		result[3][0] = -glm::dot(s, eye);
		result[3][1] = -glm::dot(u, eye);
		result[3][2] = +glm::dot(f, eye);

		return result;
	}

	//
	// QuaternionLookAt
	//
	ATLAS_API glm::quat QuaternionLookAt(const glm::vec3& eye, const glm::vec3& center, const glm::vec3& up)
	{

		if (glm::length(center - eye) < Constants::Epsilon)
		{
			return glm::quat();
		}

		const glm::vec3 forward(glm::normalize(center - eye));
		const glm::vec3 side(glm::normalize(glm::cross(forward, up)));
		const glm::vec3 relativeUp(glm::cross(side, forward));
		const glm::vec3 referenceUp(glm::normalize(up));

		const f32 m = std::sqrt(2.0f + 2.0f * glm::dot(relativeUp, referenceUp));
		glm::vec3 v = (1.0f / m) * glm::cross(relativeUp, referenceUp);
		return glm::quat(m * 0.5f, v);

		//auto f = glm::normalize(center - eye);
		//auto cosTheta = glm::dot(up, f);

		//auto angle = glm::acos(cosTheta);
		//auto axis = glm::cross(up, f);
		//return glm::angleAxis(angle, axis);

	}
} // Atlas