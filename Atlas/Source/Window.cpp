#include <Atlas/Window.hpp>
#include <Atlas/Graphics/OpenGL.hpp>
#include <Atlas/Event.hpp>

#include <iostream>
#include <memory>

namespace Atlas
{
	namespace Window
	{
		SDL_Window* g_Ptr = nullptr;
		SDL_GLContext g_ContextPtr = nullptr;
		bool g_Fullscreen = false;

		namespace
		{
			GLOBAL const i32 g_WindowWidth = 1280;
			GLOBAL const i32 g_WindowHeight = 720;

			GLOBAL i32 g_Width = g_WindowWidth;
			GLOBAL i32 g_Height = g_WindowHeight;
		} // namespace (anonymous)

		//
		// Forward Declarations
		//
		void InitializeOpenGL();
		INTERNAL Input::Key ConvertFromSDL_ScanCode(u32 code);

		// ======================
		// Window Functions
		// ======================

		//
		// CleanUp
		//
		ATLAS_API void CleanUp()
		{
			DestroyWindow();
			SDL_Quit();
		}

		INTERNAL void convertEvent(SDL_Event& e, Event& event)
		{
			switch (e.type)
			{
			case SDL_WINDOWEVENT:
			{
				if (e.window.event == SDL_WINDOWEVENT_RESIZED)
				{
					event.type = Event::Resized;
					event.size.width = e.window.data1;
					event.size.height = e.window.data2;
					return;
				}

				if (e.window.event == SDL_WINDOWEVENT_CLOSE)
				{
					event.type = Event::Closed;
					return;
				}

				if (e.window.event == SDL_WINDOWEVENT_FOCUS_GAINED)
				{
					event.type = Event::GainedFocus;
					return;
				}

				if (e.window.event == SDL_WINDOWEVENT_FOCUS_LOST)
				{
					event.type = Event::LostFocus;
					return;
				}

				if (e.window.event == SDL_WINDOWEVENT_MOVED)
				{
					event.type = Event::Moved;
					event.move.x = e.window.data1;
					event.move.y = e.window.data2;

					return;
				}
			}

			case SDL_KEYDOWN:
			{
				event.type = Event::KeyPressed;
				event.key.code = ConvertFromSDL_ScanCode(e.key.keysym.scancode);
				u16 mod{ e.key.keysym.mod };
				if (mod & KMOD_ALT)
					event.key.alt = true;
				if (mod & KMOD_CTRL)
					event.key.ctrl = true;
				if (mod & KMOD_SHIFT)
					event.key.shift = true;
				if (mod & KMOD_GUI)
					event.key.system = true;
				if (mod & KMOD_CAPS)
					event.key.capsLock = true;
				if (mod & KMOD_NUM)
					event.key.numLock = true;
				return;
			}

			case SDL_KEYUP:
			{
				event.type = Event::KeyReleased;
				event.key.code = ConvertFromSDL_ScanCode(e.key.keysym.scancode);
				u16 mod{ e.key.keysym.mod };
				if (mod & KMOD_ALT)
					event.key.alt = true;
				if (mod & KMOD_CTRL)
					event.key.ctrl = true;
				if (mod & KMOD_SHIFT)
					event.key.shift = true;
				if (mod & KMOD_GUI)
					event.key.system = true;
				if (mod & KMOD_CAPS)
					event.key.capsLock = true;
				if (mod & KMOD_NUM)
					event.key.numLock = true;
				return;
			}

			case SDL_MOUSEMOTION:
			{
				event.type = Event::MouseMoved;
				event.mouseMove.x = e.motion.x;
				event.mouseMove.y = e.motion.y;
				return;
			}

			case SDL_MOUSEBUTTONDOWN:
			{
				event.type = Event::MouseButtonPressed;
				event.mouseButton.button = (Input::Mouse)e.button.button;
				event.mouseButton.clicks = e.button.clicks;
				event.mouseMove.x = e.button.x;
				event.mouseMove.y = e.button.y;
				return;
			}

			case SDL_MOUSEBUTTONUP:
			{
				event.type = Event::MouseButtonReleased;
				event.mouseButton.button = (Input::Mouse)e.button.button;
				event.mouseButton.clicks = e.button.clicks;
				event.mouseMove.x = e.button.x;
				event.mouseMove.y = e.button.y;
				return;
			}

			case SDL_MOUSEWHEEL:
			{
				event.type = Event::MouseWheelScrolled;
				event.mouseWheelScroll.deltaX = e.wheel.x;
				event.mouseWheelScroll.deltaY = e.wheel.y;
				return;
			}

			case SDL_CONTROLLERBUTTONDOWN:
			{
				event.type = Event::ControllerButtonPressed;
				event.controllerButton.index = e.cbutton.which;
				event.controllerButton.button = (Input::ControllerButton)e.cbutton.button;
				return;
			}

			case SDL_CONTROLLERBUTTONUP:
			{
				event.type = Event::ControllerButtonReleased;
				event.controllerButton.index = e.cbutton.which;
				event.controllerButton.button = (Input::ControllerButton)e.cbutton.button;
				return;
			}

			case SDL_CONTROLLERDEVICEADDED:
			{
				event.type = Event::ControllerConnected;
				event.controller.index = e.cdevice.which;
				return;
			}

			case SDL_CONTROLLERDEVICEREMOVED:
			{
				event.type = Event::ControllerDisconnected;
				event.controller.index = e.cdevice.which;
				return;
			}

			case SDL_CONTROLLERDEVICEREMAPPED:
			{
				event.type = Event::ControllerRemapped;
				event.controller.index = e.cdevice.which;
				return;
			}

			case SDL_CONTROLLERAXISMOTION:
			{
				event.type = Event::ControllerAxisMoved;
				event.controllerAxis.index = e.caxis.which;
				event.controllerAxis.axis = (Input::ControllerAxis)e.caxis.axis;

				i16 value = e.caxis.value;
				if (event.controllerAxis.axis == Input::ControllerAxis::LeftY)
				{
					value = -value;
				}
				if (value >= 0)
				{
					event.controllerAxis.value = static_cast<f32>(value) / 32767.0f;
				}
				event.controllerAxis.value = static_cast<f32>(value) / 32768.0f;
				return;
			}

			default:
			{
				event.type = Event::Unknown;
				return;
			}
			}
		}

		INTERNAL Input::Key ConvertFromSDL_ScanCode(u32 code)
		{
			// clang-format off
			using namespace Input;
			switch (code)
			{
			default:                        return Key::Unknown;
			case SDL_SCANCODE_A:            return Key::A;
			case SDL_SCANCODE_B:            return Key::B;
			case SDL_SCANCODE_C:            return Key::C;
			case SDL_SCANCODE_D:            return Key::D;
			case SDL_SCANCODE_E:            return Key::E;
			case SDL_SCANCODE_F:            return Key::F;
			case SDL_SCANCODE_G:            return Key::G;
			case SDL_SCANCODE_H:            return Key::H;
			case SDL_SCANCODE_I:            return Key::I;
			case SDL_SCANCODE_J:            return Key::J;
			case SDL_SCANCODE_K:            return Key::K;
			case SDL_SCANCODE_L:            return Key::L;
			case SDL_SCANCODE_M:            return Key::M;
			case SDL_SCANCODE_N:            return Key::N;
			case SDL_SCANCODE_O:            return Key::O;
			case SDL_SCANCODE_P:            return Key::P;
			case SDL_SCANCODE_Q:            return Key::Q;
			case SDL_SCANCODE_R:            return Key::R;
			case SDL_SCANCODE_S:            return Key::S;
			case SDL_SCANCODE_T:            return Key::T;
			case SDL_SCANCODE_U:            return Key::U;
			case SDL_SCANCODE_V:            return Key::V;
			case SDL_SCANCODE_W:            return Key::W;
			case SDL_SCANCODE_X:            return Key::X;
			case SDL_SCANCODE_Y:            return Key::Y;
			case SDL_SCANCODE_Z:            return Key::Z;
			case SDL_SCANCODE_0:            return Key::Num0;
			case SDL_SCANCODE_1:            return Key::Num1;
			case SDL_SCANCODE_2:            return Key::Num2;
			case SDL_SCANCODE_3:            return Key::Num3;
			case SDL_SCANCODE_4:            return Key::Num4;
			case SDL_SCANCODE_5:            return Key::Num5;
			case SDL_SCANCODE_6:            return Key::Num6;
			case SDL_SCANCODE_7:            return Key::Num7;
			case SDL_SCANCODE_8:            return Key::Num8;
			case SDL_SCANCODE_9:            return Key::Num9;
			case SDL_SCANCODE_ESCAPE:       return Key::Escape;
			case SDL_SCANCODE_LCTRL:        return Key::LControl;
			case SDL_SCANCODE_LSHIFT:       return Key::LShift;
			case SDL_SCANCODE_LALT:         return Key::LAlt;
			case SDL_SCANCODE_LGUI:         return Key::LSystem;
			case SDL_SCANCODE_RCTRL:        return Key::RControl;
			case SDL_SCANCODE_RSHIFT:       return Key::RShift;
			case SDL_SCANCODE_RALT:         return Key::RAlt;
			case SDL_SCANCODE_RGUI:         return Key::RSystem;
			case SDL_SCANCODE_MENU:         return Key::Menu;
			case SDL_SCANCODE_LEFTBRACKET:  return Key::LBracket;
			case SDL_SCANCODE_RIGHTBRACKET: return Key::RBracket;
			case SDL_SCANCODE_SEMICOLON:    return Key::SemiColon;
			case SDL_SCANCODE_COMMA:        return Key::Comma;
			case SDL_SCANCODE_PERIOD:       return Key::Period;
			case SDL_SCANCODE_APOSTROPHE:   return Key::Apostrophe;
			case SDL_SCANCODE_SLASH:        return Key::Slash;
			case SDL_SCANCODE_BACKSLASH:    return Key::BackSlash;
			case SDL_SCANCODE_EQUALS:       return Key::Equal;
			case SDL_SCANCODE_MINUS:        return Key::Minus;
			case SDL_SCANCODE_SPACE:        return Key::Space;
			case SDL_SCANCODE_RETURN:       return Key::Return;
			case SDL_SCANCODE_BACKSPACE:    return Key::BackSpace;
			case SDL_SCANCODE_TAB:          return Key::Tab;
			case SDL_SCANCODE_GRAVE:        return Key::GraveAccent;
			case SDL_SCANCODE_PAGEUP:       return Key::PageUp;
			case SDL_SCANCODE_PAGEDOWN:     return Key::PageDown;
			case SDL_SCANCODE_END:          return Key::End;
			case SDL_SCANCODE_HOME:         return Key::Home;
			case SDL_SCANCODE_INSERT:       return Key::Insert;
			case SDL_SCANCODE_DELETE:       return Key::Delete;
			case SDL_SCANCODE_KP_PLUS:      return Key::Add;
			case SDL_SCANCODE_KP_MINUS:     return Key::Subtract;
			case SDL_SCANCODE_KP_MULTIPLY:  return Key::Multiply;
			case SDL_SCANCODE_KP_DIVIDE:    return Key::Divide;
			case SDL_SCANCODE_LEFT:         return Key::Left;
			case SDL_SCANCODE_RIGHT:        return Key::Right;
			case SDL_SCANCODE_UP:           return Key::Up;
			case SDL_SCANCODE_DOWN:         return Key::Down;
			case SDL_SCANCODE_KP_0:         return Key::Numpad0;
			case SDL_SCANCODE_KP_1:         return Key::Numpad1;
			case SDL_SCANCODE_KP_2:         return Key::Numpad2;
			case SDL_SCANCODE_KP_3:         return Key::Numpad3;
			case SDL_SCANCODE_KP_4:         return Key::Numpad4;
			case SDL_SCANCODE_KP_5:         return Key::Numpad5;
			case SDL_SCANCODE_KP_6:         return Key::Numpad6;
			case SDL_SCANCODE_KP_7:         return Key::Numpad7;
			case SDL_SCANCODE_KP_8:         return Key::Numpad8;
			case SDL_SCANCODE_KP_9:         return Key::Numpad9;
			case SDL_SCANCODE_F1:           return Key::F1;
			case SDL_SCANCODE_F2:           return Key::F2;
			case SDL_SCANCODE_F3:           return Key::F3;
			case SDL_SCANCODE_F4:           return Key::F4;
			case SDL_SCANCODE_F5:           return Key::F5;
			case SDL_SCANCODE_F6:           return Key::F6;
			case SDL_SCANCODE_F7:           return Key::F7;
			case SDL_SCANCODE_F8:           return Key::F8;
			case SDL_SCANCODE_F9:           return Key::F9;
			case SDL_SCANCODE_F10:          return Key::F10;
			case SDL_SCANCODE_F11:          return Key::F11;
			case SDL_SCANCODE_F12:          return Key::F12;
			case SDL_SCANCODE_F13:          return Key::F13;
			case SDL_SCANCODE_F14:          return Key::F14;
			case SDL_SCANCODE_F15:          return Key::F15;
			case SDL_SCANCODE_PAUSE:        return Key::Pause;
			}
			// clang-format on
		}

		ATLAS_API SDL_Window* CreateWindow(i32 monitor)
		{
			auto window = CreateWindow(monitor, g_WindowWidth, g_WindowHeight);
			if (window == nullptr)
			{
				throw new std::exception("SDL_CreateWindow returned nullptr");
			}
			return window;
		}

		ATLAS_API SDL_Window* CreateWindow(i32 monitor, u32 width, u32 height)
		{
			auto window = SDL_CreateWindow("Atlas", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, Window::g_Width, Window::g_Height, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
			if (window == nullptr)
			{
				throw new std::exception("SDL_CreateWindow returned nullptr");
			}
			g_Width = width;
			g_Height = height;
			return window;
		}

		//
		//
		//
		ATLAS_API void DestroyWindow()
		{
			DestroyWindow(Window::g_Ptr);
		}

		//
		//
		//
		ATLAS_API void DestroyWindow(SDL_Window* window)
		{
			if (g_ContextPtr != nullptr)
			{
				SDL_GL_DeleteContext(g_ContextPtr);
			}
			if (window != nullptr)
			{
				SDL_DestroyWindow(window);
			}
		}

		//
		//
		//
		ATLAS_API glm::vec2 GetWindowSize()
		{
			return glm::vec2(static_cast<f32>(Window::g_Width), static_cast<f32>(Window::g_Height));
		}

		//
		//
		//
		ATLAS_API glm::vec2 GetFramebufferSize()
		{
			i32 width;
			i32 height;

			SDL_GetWindowSize(g_Ptr, &width, &height);

			return glm::vec2(width, height);
		}

		//
		// Initialize
		//
		ATLAS_API bool Initialize()
		{
			auto initResult = SDL_Init(SDL_INIT_EVERYTHING);
			if (initResult != 0)
			{
				auto errorMessage = SDL_GetError();
				std::cout << "SDL: Unable to initialize, Details: " << errorMessage << "\n";
			}
			std::cout << "SDL: Initialized.\n";
			Window::g_Ptr = CreateWindow(0, g_WindowWidth, g_WindowHeight);
			if (Window::g_Ptr == nullptr)
			{
				return false;
			}
			std::cout << "Window: Created.\n";

			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
			//SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
			SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
			SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 32);
			SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
#if _DEBUG
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

			Window::g_ContextPtr = SDL_GL_CreateContext(Window::g_Ptr);
			if (Window::g_ContextPtr == nullptr)
			{
				auto errorMessage = SDL_GetError();
				std::cout << "SDL: Unable to create OpenGL context, Details: " << errorMessage << "\n";
			}
			std::cout << "SDL: OpenGL context created.\n";
			InitializeOpenGL();
			MakeContextCurrent();
			SetSwapInterval(0);
			return true;
		}

		//
		//
		//
		void InitializeOpenGL()
		{
			glewExperimental = GL_TRUE;
			if (glewInit() != GLEW_OK)
			{
				std::cout << "GLEW: Unable to initialize GLEW.\n";
			}

			glViewport(0, 0, g_Width, g_Height);
			glEnable(GL_FRAMEBUFFER_SRGB); // that will take care of gamma correction automagically
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			glFrontFace(GL_CCW);
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LEQUAL);

			glClearColor(0.0f, 0.1f, 0.4f, 1.0f);
			std::cout << "SDL: OpenGL initialized.\n";
		}

		//
		//
		//
		ATLAS_API void MakeContextCurrent()
		{
			SDL_GL_MakeCurrent(Window::g_Ptr, Window::g_ContextPtr);
		}

		//
		//
		//
		ATLAS_API bool Window::PollEvent(Event& event)
		{
			SDL_Event e;
			if (SDL_PollEvent(&e))
			{
				convertEvent(e, event);
				return true;
			}

			return false;
		}

		//
		//
		//
		ATLAS_API void SetSwapInterval(i32 interval)
		{
			SDL_GL_SetSwapInterval(interval);
		}

		//
		//
		//
		ATLAS_API void SetTitle(const char* title)
		{
			SDL_SetWindowTitle(Window::g_Ptr, title);
		}

		//
		//
		//
		ATLAS_API void SwapBuffers()
		{
			SDL_GL_SwapWindow(Window::g_Ptr);
		}
	} // Window
} // Atlas