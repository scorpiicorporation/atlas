#include <Atlas/Graphics/RenderTarget.hpp>

#include <vector>

namespace Atlas
{
	namespace Graphics
	{
		//
		// RenderTarget - Ctor
		//
		RenderTarget::RenderTarget()
			: colorTexture{}
			, depthTexture{}
			, _width{ 0 }
			, _height{ 0 }
			, _type{ Color }
			, _fbo{ 0 }
		{
		}

		//
		// RenderTarget - Dtor
		//
		RenderTarget::~RenderTarget()
		{
			if (_fbo)
			{
				glDeleteFramebuffersEXT(1, &_fbo);
			}
		}

		//
		// Create
		//
		bool RenderTarget::Create(i32 w, i32 h, TextureType t, TextureFilter minMagFilter, TextureWrap wrapMode)
		{
			if (w == _width && h == _height && t == _type)
			{
				return true;
			}
			_type = t;
			_width = w;
			_height = h;

			if (!_fbo)
			{
				glGenFramebuffersEXT(1, &_fbo);
			}
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, _fbo);

			if ((_type & TextureType::Color) == TextureType::Color)
			{
				auto nativeHandle = colorTexture.GetNativeHandle();

				glBindTexture(GL_TEXTURE_2D, nativeHandle);
				if ((_type & Lighting) == TextureType::Lighting)
				{
					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, _width, _height, 0, GL_RGB, GL_FLOAT, 0);
				}
				else
				{
					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _width, _height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
				}
				colorTexture.SetWidth(_width);
				colorTexture.SetHeight(_height);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (GLenum)minMagFilter);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (GLenum)minMagFilter);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (GLenum)wrapMode);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (GLenum)wrapMode);

				glFramebufferTextureEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, nativeHandle, 0);
			}

			if ((_type & TextureType::Depth) == TextureType::Depth)
			{
				auto nativeHandle = depthTexture.GetNativeHandle();

				glBindTexture(GL_TEXTURE_2D, (GLuint)nativeHandle);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, _width, _height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
				depthTexture.SetWidth(_width);
				depthTexture.SetHeight(_height);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (GLenum)minMagFilter);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (GLenum)minMagFilter);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (GLenum)wrapMode);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (GLenum)wrapMode);

				glFramebufferTextureEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, nativeHandle, 0);
			}

			std::vector<GLenum> drawBuffers;
			if (((_type & TextureType::Color) == TextureType::Color) || ((_type & TextureType::Lighting) == TextureType::Lighting))
			{
				drawBuffers.push_back(GL_COLOR_ATTACHMENT0_EXT);
			}
			if ((_type & TextureType::Depth) == TextureType::Depth)
			{
				drawBuffers.push_back(GL_DEPTH_ATTACHMENT);
			}
			glDrawBuffers(static_cast<i32>(drawBuffers.size()), &drawBuffers[0]);

			if (glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT) != GL_FRAMEBUFFER_COMPLETE_EXT)
			{
				glBindTexture(GL_TEXTURE_2D, 0);
				glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
				return false;
			}

			glBindTexture(GL_TEXTURE_2D, 0);
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
			return true;
		}

		//
		// Bind
		//
		void RenderTarget::Bind(const RenderTarget* renderTarget)
		{
			if (renderTarget == nullptr)
			{
				glFlush();
			}
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, renderTarget != nullptr ? renderTarget->GetNativeHandle() : 0);
		}

		//
		// GetWidth
		//
		i32 RenderTarget::GetWidth() const
		{
			return _width;
		}

		//
		// GetHeight
		//
		i32 RenderTarget::GetHeight() const
		{
			return _height;
		}

		//
		// GetType
		//
		RenderTarget::TextureType RenderTarget::GetType() const
		{
			return _type;
		}

		//
		// GetNativeHandle
		//
		GLuint RenderTarget::GetNativeHandle() const
		{
			return _fbo;
		}
	} // Graphics
} // Atlas