#include <Atlas/Graphics/GBuffer.hpp>

#include <vector>

namespace Atlas
{
	namespace Graphics
	{
		//
		// GBuffer - Ctor
		//
		GBuffer::GBuffer() {}

		//
		// GBuffer - Dtor
		//
		GBuffer::~GBuffer()
		{
			if (_nativeHandle)
			{
				glDeleteFramebuffersEXT(1, &_nativeHandle);
			}
		}

		//
		// Create
		//
		bool GBuffer::Create(u32 w, u32 h)
		{
			if (w == _width && h == _height)
			{
				return true;
			}
			_width = w;
			_height = h;

			if (!_nativeHandle)
			{
				glGenFramebuffersEXT(1, &_nativeHandle);
			}
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, _nativeHandle);

			GLuint depthRenderBuffer = 0;
			glGenRenderbuffersEXT(1, &depthRenderBuffer);
			glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depthRenderBuffer);
			glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, (GLsizei)_width, (GLsizei)_height);
			glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER_EXT, depthRenderBuffer);

			std::vector<GLenum> drawBuffers;

			auto addRenderTarget = [&drawBuffers, w, h](Texture& texture, GLenum attachment, GLint internalFormat, GLenum format, GLenum type)
			{
				auto nativeHandle = texture.GetNativeHandle();

				glBindTexture(GL_TEXTURE_2D, nativeHandle);
				glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, (GLsizei)w, (GLsizei)h, 0, format, type, nullptr);
				texture.SetWidth(w);
				texture.SetHeight(h);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

				glFramebufferTextureEXT(GL_FRAMEBUFFER_EXT, attachment, nativeHandle, 0);

				if (attachment != GL_DEPTH_ATTACHMENT_EXT)
				{
					drawBuffers.push_back(attachment);
				}
			};

			addRenderTarget(Diffuse, GL_COLOR_ATTACHMENT0_EXT, GL_RGB8, GL_RGB, GL_UNSIGNED_BYTE);
			addRenderTarget(Specular, GL_COLOR_ATTACHMENT1_EXT, GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE);
			addRenderTarget(Normal, GL_COLOR_ATTACHMENT2_EXT, GL_RGB10_A2, GL_RGBA, GL_FLOAT);
			addRenderTarget(Depth, GL_DEPTH_ATTACHMENT_EXT, GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT, GL_FLOAT);

			glDrawBuffers(drawBuffers.size(), &drawBuffers[0]);

			if (glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT) != GL_FRAMEBUFFER_COMPLETE_EXT)
			{
				glBindTexture(GL_TEXTURE_2D, 0);
				glBindFramebuffer(GL_FRAMEBUFFER_EXT, 0);
				return false;
			}

			glBindTexture(GL_TEXTURE_2D, 0);
			glBindFramebuffer(GL_FRAMEBUFFER_EXT, 0);
			return true;
		}

		//
		// Bind
		//
		void GBuffer::Bind(const GBuffer* buffer)
		{
			if (!buffer)
			{
				glFlush();
			}
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, buffer != nullptr ? buffer->_nativeHandle : 0);
		}

		//
		// GetWidth
		//
		u32 GBuffer::GetWidth() const
		{
			return _width;
		}

		//
		// GetHeight
		//
		u32 GBuffer::GetHeight() const
		{
			return _height;
		}

		//
		// GetNativeHandle
		//
		GLuint GBuffer::GetNativeHandle() const
		{
			return _nativeHandle;
		}
	} // Graphics
} // Atlas