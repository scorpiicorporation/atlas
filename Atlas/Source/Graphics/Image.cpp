#include <Atlas/Graphics/Image.hpp>

#include <stb_image.h>

#include <iostream>

namespace Atlas
{
	namespace Graphics
	{
		//
		// Image - Ctor
		//
		Image::Image()
			: _format(ImageFormat::None), _width(0), _height(0), _pixels(nullptr)
		{
		}

		//
		// Image - Ctor
		//
		Image::Image(u32 width, u32 height, ImageFormat format, const u8* pixels)
			: _format(ImageFormat::None), _width(0), _height(0), _pixels(nullptr)
		{
			LoadFromMemory(width, height, format, pixels);
		}

		//
		// Image - Ctor
		//
		Image::Image(const Image& other)
			: _format(ImageFormat::None), _width(0), _height(0), _pixels(nullptr)
		{
			LoadFromMemory(other._width, other._height, other._format, other._pixels);
		}

		//
		// operator=
		//
		Image& Image::operator=(const Image& other)
		{
			LoadFromMemory(other._width, other._height, other._format, other._pixels);
			return *this;
		}

		// 
		// Image - Dtor
		//
		Image::~Image()
		{
			if (_pixels != nullptr)
			{
				delete[] _pixels;
			}
		}

		//
		// LoadFromFile
		//
		bool Image::LoadFromFile(const char* fileName)
		{
			i32 width;
			i32 height;
			i32 channels;

			u8* pixels = stbi_load(fileName, &width, &height, &channels, 0);
			if (pixels == nullptr)
			{
				std::cout << "Image: Failed loading \"" << fileName << "\" Reason: " << stbi_failure_reason() << "\n";
				return false;
			}
			LoadFromMemory(width, height, (ImageFormat)channels, pixels);
			stbi_image_free(pixels);
			if (_pixels)
			{
				return true;
			}
			return false;
		}

		//
		// LoadFromMemory
		//
		bool Image::LoadFromMemory(u32 width, u32 height, ImageFormat format, const u8* pixels)
		{
			if (width == 0 || height == 0)
			{
				std::cout << "Image: Zero Image Dimension (Width or Height)\n";
				return false;
			}

			_width = width;
			_height = height;
			_format = format;

			auto size = width * height * (i32)format;
			if (_pixels != nullptr)
			{
				delete[] _pixels;
			}
			_pixels = new u8[size];

			if (pixels != nullptr)
			{
				std::memcpy(_pixels, pixels, size);
			}
			return true;
		}

		//
		// FlipVertically
		//
		void Image::FlipVertically()
		{
			auto pitch = _width * (i32)_format;
			auto halfRows = _height / 2;
			auto rowBuffer = new u8[pitch];

			for (auto i = 0; i < halfRows; ++i)
			{
				auto row = _pixels + (i * _width) * (i32)_format;
				auto oppositeRow = _pixels + ((_height - i - 1) * _width) * (i32)_format;

				std::memcpy(rowBuffer, row, pitch);
				std::memcpy(row, oppositeRow, pitch);
				std::memcpy(oppositeRow, rowBuffer, pitch);
			}

			delete[] rowBuffer;
		}
	} // Graphics
} // Atlas