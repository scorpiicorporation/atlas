#include <Atlas/Graphics/Texture.hpp>
#include <iostream>

namespace Atlas
{
	namespace Graphics
	{
		//
		// GetInternalFormatFromFormat
		//
		INTERNAL GLenum GetInternalFormatFromFormat(ImageFormat format, bool sRGB)
		{
			switch (format)
			{
			case ImageFormat::Greyscale:
				return GL_LUMINANCE;
			case ImageFormat::GreyscaleAlpha:
				return GL_LUMINANCE_ALPHA;
			case ImageFormat::RGB:
				return (sRGB ? GL_SRGB : GL_RGB);
			case ImageFormat::RGBA:
				return (sRGB ? GL_SRGB_ALPHA : GL_RGBA);
			case ImageFormat::None:
			default:
				std::cout << "Texture: No ImageFormat Set.\n";
				return 0;
			}
		}

		//
		// Texture - Ctor
		//
		Texture::Texture()
			: _nativeHandle(0), _width(0), _height(0)
		{
			glGenTextures(1, &_nativeHandle);
		}

		//
		// Texture - Ctor
		//
		Texture::Texture(const Image& image, TextureFilter textureFilter, TextureWrap textureWrap)
			: _nativeHandle(0), _width(image.GetWidth()), _height(image.GetHeight())
		{
			if (!LoadFromImage(image, textureFilter, textureWrap))
			{
				throw new std::runtime_error("Could not create texture from image.");
			}
		}

		//
		// Texture - Dtor
		//
		Texture::~Texture()
		{
			std::cout << "Texture: Destroying Handle " << _nativeHandle << "\n";
			glDeleteTextures(1, &_nativeHandle);
		}

		//
		// LoadFromFile
		//
		bool Texture::LoadFromFile(const char* fileName, TextureFilter textureFilter, TextureWrap textureWrap)
		{
			Image image;
			if (!image.LoadFromFile(fileName))
			{
				std::cout << "Texture: \"" << fileName << "\" not found.\n";
				return false;
			}
#if _DEBUG
			std::cout << "Texture: Loaded ... " << fileName << "\n";
#endif
			image.FlipVertically();

			return LoadFromImage(image, textureFilter, textureWrap);
		}

		//
		// LoadFromImage
		//
		bool Texture::LoadFromImage(const Image& image, TextureFilter textureFilter, TextureWrap textureWrap)
		{
			_width = image.GetWidth();
			_height = image.GetHeight();

			glGenTextures(1, &_nativeHandle);
			glBindTexture(GL_TEXTURE_2D, _nativeHandle);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (i32)textureWrap);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (i32)textureWrap);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (i32)textureFilter);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (i32)textureFilter);

			auto internalFormat = GetInternalFormatFromFormat(image.GetFormat(), true);
			auto format = GetInternalFormatFromFormat(image.GetFormat(), false);
			glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, (i32)_width, (i32)_height, 0, format, GL_UNSIGNED_BYTE, image.GetPixels());
			glBindTexture(GL_TEXTURE_2D, 0);
			return true;
		}

		//
		// Bind
		//
		void Texture::Bind(const Texture* texture, u32 position)
		{
			if (position > 31)
			{
				std::cout << "Maximum Texture Limit Reached." << std::endl;
				position = 31;
			}
			glActiveTexture(GL_TEXTURE0 + position);
			glClientActiveTexture(GL_TEXTURE0 + position);

			glEnable(GL_TEXTURE_2D);
			if (texture == nullptr)
			{
				glBindTexture(GL_TEXTURE_2D, 0);
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, texture->GetNativeHandle() ? texture->GetNativeHandle() : 0);
			}
			glDisable(GL_TEXTURE_2D);
		}
	} // Graphics
} // Atlas