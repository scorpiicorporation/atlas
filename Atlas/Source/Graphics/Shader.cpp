#include <Atlas/Graphics/Shader.hpp>

#include <glm/gtc/type_ptr.hpp>

#include <vector>

namespace Atlas
{
	namespace Graphics
	{
		//
		// SplitString
		//
		std::vector<std::string> SplitString(const std::string& s, char delim)
		{
			std::vector<std::string> result;

			const char* cstr{ s.c_str() };
			auto length = s.length();
			u32 start{ 0 };
			u32 end{ 0 };

			while (end <= length)
			{
				while (end <= length)
				{
					if (cstr[end] == delim)
					{
						break;
					}
					end++;
				}

				result.emplace_back(s.substr(start, end - start));
				start = end + 1;
				end = start;
			}

			return result;
		}

		//
		// GetFileDirectory
		//
		std::string GetFileDirectory(const std::string& filepath)
		{
			auto found = filepath.find_last_of("/\\");
			return filepath.substr(0, found);
		}

		//
		// ReadStringFromFile
		//
		INTERNAL std::string ReadStringFromFile(const std::string& filename)
		{
			std::ifstream file;
			file.open(std::string{ std::string("Data/Shaders/" + filename).c_str() }, std::ios::in | std::ios::binary);

			std::string fileDirectory{ GetFileDirectory(filename) + "/" };

			std::string output;
			std::string line;

			if (!file.is_open())
			{
				std::cout << "Shader: Failed to open file : " << filename << "\n";
			}
			else
			{
				while (file.good())
				{
					std::getline(file, line);

					if (line.find("#include") == std::string::npos)
					{
						output.append(line + "\n");
					}
					else
					{
						std::string includeFilename{ SplitString(line, ' ')[1] };

						if (includeFilename[0] == '<') // Absolute Path (library path)
						{
							u32 closingBracketPos{ 0 };
							for (u32 i{ 1 }; i < includeFilename.length(); i++)
							{
								if (includeFilename[i] == '>')
								{
									closingBracketPos = i;
									break;
								}
							}

							if (closingBracketPos > 1)
							{
								includeFilename = includeFilename.substr(1, closingBracketPos - 1);
							}
							else
							{
								includeFilename = "";
							}
						}
						else if (includeFilename[0] == '\"') // Relative Path (folder path)
						{
							u32 closingSpeechMark{ 0 };
							for (u32 i{ 1 }; i < includeFilename.length(); i++)
							{
								if (includeFilename[i] == '\"')
								{
									closingSpeechMark = i;
									break;
								}
							}

							if (closingSpeechMark > 1)
							{
								includeFilename = includeFilename.substr(1, closingSpeechMark - 1);
							}
							else
							{
								includeFilename = "";
							}
						}

						std::cout << "Shader: Include GLSL - \"" << includeFilename << "\"\n";
						if (includeFilename.length() > 0)
						{
							output.append(ReadStringFromFile(includeFilename) + "\n");
						}
					}
				}
			}

			file.close();
			return output;
		}

		//
		// Shader - Ctor
		//
		Shader::Shader() 
			: _programHandle(0), _isLinked(false), _errorLog()
		{
			_programHandle = glCreateProgram();
		}

		//
		// Shader - Dtor
		//
		Shader::~Shader()
		{
			if (_programHandle != 0)
			{
				glDeleteProgram(_programHandle);
			}
		}

		//
		// AttachShaderFromFile
		//
		bool Shader::AttachShaderFromFile(ShaderType shaderType, const std::string& fileName)
		{
			auto shaderSource = ReadStringFromFile(fileName);
#if _DEBUG
			std::cout << "Shader: Loading... " << fileName << "\n";
#endif
			return AttachShaderFromMemory(shaderType, shaderSource);
		}

		//
		// AttachShaderFromMemory
		//
		bool Shader::AttachShaderFromMemory(ShaderType shaderType, const std::string& shaderSource)
		{
			if (_programHandle == 0)
			{
				_programHandle = glCreateProgram();
			}

			auto shaderText = shaderSource.c_str();

			GLuint shaderHandle;

			if (shaderType == ShaderType::VertexShader)
			{
				shaderHandle = glCreateShader(GL_VERTEX_SHADER);
			}
			else if (shaderType == ShaderType::FragmentShader)
			{
				shaderHandle = glCreateShader(GL_FRAGMENT_SHADER);
			}

			glShaderSource(shaderHandle, 1, &shaderText, nullptr);
			glCompileShader(shaderHandle);

			GLint compileStatus;
			glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileStatus);
			if (compileStatus == GL_FALSE)
			{
				GLint errorMessageLength;
				glGetShaderiv(shaderHandle, GL_INFO_LOG_LENGTH, &errorMessageLength);
				char* errorMessage = new char[errorMessageLength + 1];
				glGetShaderInfoLog(shaderHandle, errorMessageLength, nullptr, errorMessage);

				_errorLog.append("Shader: Compile Failure:\n");
				_errorLog.append(errorMessage);
				delete[] errorMessage;

				glDeleteShader(shaderHandle);
				return false;
			}

			glAttachShader(_programHandle, shaderHandle);
			glDeleteShader(shaderHandle);
			//TODO(deccer): maybe serialize the compiled blob and make use of it next time the program starts

			return true;
		}

		//
		// Apply
		//
		void Shader::Apply() const
		{
			if (!IsApplied())
			{
				glUseProgram(_programHandle);
			}
		}

		//
		// Unapply
		//
		void Shader::Unapply() const
		{
			if (IsApplied())
			{
				glUseProgram(0);
			}
		}

		//
		// IsApplied - TODO(deccer) - only call in SetUniform - when in _DEBUG
		//
		bool Shader::IsApplied() const
		{
			GLint currentProgramHandle = 0;
			glGetIntegerv(GL_CURRENT_PROGRAM, &currentProgramHandle);
			return (GLint)_programHandle == currentProgramHandle;
		}

		//
		// IsBound
		//
		void Shader::IsBound() const
		{
			if (!IsApplied())
			{
				std::cout << "Shader: Not bound.\n";
			}
		}

		//
		// Link
		//
		bool Shader::Link()
		{
			if (_programHandle == 0)
			{
				_programHandle = glCreateProgram();
			}
			if (!_isLinked)
			{
				glLinkProgram(_programHandle);
				GLint linkStatus;

				glGetProgramiv(_programHandle, GL_LINK_STATUS, &linkStatus);
				if (linkStatus == GL_FALSE)
				{
					GLint errorMessageLength;
					glGetProgramiv(_programHandle, GL_INFO_LOG_LENGTH, &errorMessageLength);
					char* errorMessage = new char[errorMessageLength + 1];
					glGetProgramInfoLog(_programHandle, errorMessageLength, nullptr, errorMessage);

					_errorLog.append("Shader: Linking Failure:\n");
					_errorLog.append(errorMessage);
					delete[] errorMessage;

					glDeleteProgram(_programHandle);
					_programHandle = 0;

					_isLinked = false;

					return _isLinked;
				}
				_isLinked = true;
			}
			return _isLinked;
		}

		//
		// BindAttributeLocation
		//
		void Shader::BindAttributeLocation(GLuint location, const std::string& attributeName)
		{
			glBindAttribLocation(_programHandle, location, attributeName.c_str());
			_attributeLocations[attributeName] = location;
		}

		//
		// GetAttributeLocation
		//
		GLint Shader::GetAttributeLocation(const std::string& attributeName)
		{
			auto found = _attributeLocations.find(attributeName);
			if (found != _attributeLocations.end())
			{
				return found->second;
			}

			auto location = glGetAttribLocation(_programHandle, attributeName.c_str());
			_attributeLocations[attributeName] = location;
			return location;
		}

		//
		// GetUniformLocation
		//
		GLint Shader::GetUniformLocation(const std::string& uniformName)
		{
			auto found = _uniformLocations.find(uniformName);
			if (found != _uniformLocations.end())
			{
				return found->second;
			}

			auto location = glGetUniformLocation(_programHandle, uniformName.c_str());
			_uniformLocations[uniformName] = location;
			return location;
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, f32 x)
		{
			IsBound();
			glUniform1f(GetUniformLocation(uniformName), x);
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, f32 x, f32 y)
		{
			IsBound();
			glUniform2f(GetUniformLocation(uniformName), x, y);
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, f32 x, f32 y, f32 z)
		{
			IsBound();
			glUniform3f(GetUniformLocation(uniformName), x, y, z);
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, f32 x, f32 y, f32 z, f32 w)
		{
			IsBound();
			glUniform4f(GetUniformLocation(uniformName), x, y, z, w);
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, i32 value)
		{
			IsBound();
			glUniform1i(GetUniformLocation(uniformName), value);
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, bool value)
		{
			IsBound();
			glUniform1i(GetUniformLocation(uniformName), (i32)value);
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, const glm::mat3x3& value)
		{
			IsBound();
			glUniformMatrix3fv(GetUniformLocation(uniformName), 1, GL_FALSE, glm::value_ptr(value));
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, const glm::mat4& value)
		{
			IsBound();
			glUniformMatrix4fv(GetUniformLocation(uniformName), 1, GL_FALSE, glm::value_ptr(value));
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, const glm::mat4x3& value)
		{
			IsBound();
			glUniformMatrix4x3fv(GetUniformLocation(uniformName), 1, GL_FALSE, glm::value_ptr(value));
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, const glm::vec2& value)
		{
			IsBound();
			glUniform2fv(GetUniformLocation(uniformName), 1, glm::value_ptr(value));
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, const glm::vec3& value)
		{
			IsBound();
			glUniform3fv(GetUniformLocation(uniformName), 1, glm::value_ptr(value));
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, const glm::vec4& value)
		{
			IsBound();
			glUniform4fv(GetUniformLocation(uniformName), 1, glm::value_ptr(value));
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, const glm::quat& value)
		{
			IsBound();
			glUniform4fv(GetUniformLocation(uniformName), 1, glm::value_ptr(value));
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, const Transform& value)
		{
			IsBound();

			SetUniform(uniformName + ".position", value.Position);
			SetUniform(uniformName + ".orientation", value.Orientation);
			SetUniform(uniformName + ".scale", value.Scale);
		}

		//
		// SetUniform
		//
		void Shader::SetUniform(const std::string& uniformName, const Color& value)
		{

			Atlas::f32 r{ value.r / 255.0f };
			Atlas::f32 g{ value.g / 255.0f };
			Atlas::f32 b{ value.b / 255.0f };
			Atlas::f32 a{ value.a / 255.0f };

			SetUniform(uniformName, r, g, b, a);
		}
	} // Graphics
} // Atlas