#include <Atlas/Time.hpp>

namespace Atlas
{
	const Time Time::Zero{ 0 };

	//
	// AsSeconds
	//
	f32 Time::AsSeconds() const
	{
		return _microseconds.count() / 1000000.0f;
	}

	//
	// AsMilliseconds
	//
	i32 Time::AsMilliseconds() const
	{
		return static_cast<i32>(_microseconds.count() / 1000);
	}

	//
	// AsMicroseconds
	//
	i64 Time::AsMicroseconds() const
	{
		return _microseconds.count();
	}

	//
	// Now
	//
	Time Time::Now()
	{
		auto now = std::chrono::high_resolution_clock::now().time_since_epoch();
		return SetMicroseconds(std::chrono::duration_cast<std::chrono::microseconds>(now).count());
	}

	// 
	// Sleep
	//
	void Time::Sleep(Time time)
	{
		std::this_thread::sleep_for(std::chrono::microseconds(time.AsMicroseconds()));
	}

	//
	// Time - Ctor
	//
	Time::Time(i64 microseconds)
		: _microseconds{ std::chrono::microseconds{ microseconds } }
	{
	}

	//
	// operator==
	//
	ATLAS_API bool operator==(Time left, Time right)
	{
		return left.AsMicroseconds() == right.AsMicroseconds();
	}

	//
	// operator!=
	//
	ATLAS_API bool operator!=(Time left, Time right)
	{
		return left.AsMicroseconds() != right.AsMicroseconds();
	}

	//
	// operator<
	//
	ATLAS_API bool operator<(Time left, Time right)
	{
		return left.AsMicroseconds() < right.AsMicroseconds();
	}

	//
	// operator>
	//
	ATLAS_API bool operator>(Time left, Time right)
	{
		return left.AsMicroseconds() > right.AsMicroseconds();
	}

	//
	// operator<=
	//
	ATLAS_API bool operator<=(Time left, Time right)
	{
		return left.AsMicroseconds() <= right.AsMicroseconds();
	}

	//
	// operator>=
	//
	ATLAS_API bool operator>=(Time left, Time right)
	{
		return left.AsMicroseconds() >= right.AsMicroseconds();
	}

	//
	// operator-
	//
	ATLAS_API Time operator-(Time right)
	{
		return SetMicroseconds(-right.AsMicroseconds());
	}

	//
	// operator+
	//
	ATLAS_API Time operator+(Time left, Time right)
	{
		return SetMicroseconds(left.AsMicroseconds() + right.AsMicroseconds());
	}

	//
	// operator-
	//
	ATLAS_API Time operator-(Time left, Time right)
	{
		return SetMicroseconds(left.AsMicroseconds() - right.AsMicroseconds());
	}

	//
	// operator+=
	//
	ATLAS_API Time& operator+=(Time& left, Time right)
	{ 
		return left = left + right; 
	}

	//
	// operator-=
	//
	ATLAS_API Time& operator-=(Time& left, Time right)
	{ 
		return left = left - right; 
	}

	//
	// operator*=
	//
	ATLAS_API Time operator*(Time left, f32 right)
	{
		return SetSeconds(left.AsSeconds() * right);
	}

	//
	// operator*
	//
	ATLAS_API Time operator*(Time left, i64 right)
	{
		return SetMicroseconds(left.AsMicroseconds() * right);
	}

	//
	// operator*
	//
	ATLAS_API Time operator*(f32 left, Time right)
	{
		return SetSeconds(left * right.AsSeconds());
	}

	//
	// operator*
	//
	ATLAS_API Time operator*(i64 left, Time right)
	{
		return SetMicroseconds(right.AsMicroseconds() * left);
	}

	//
	// operator==
	//
	ATLAS_API Time& operator*=(Time& left, f32 right)
	{
		return left = left * right;
	}

	//
	// operator*=
	//
	ATLAS_API Time& operator*=(Time& left, i64 right)
	{
		return left = left * right;
	}

	//
	// operator/
	//
	ATLAS_API Time operator/(Time left, f32 right)
	{
		return SetSeconds(left.AsSeconds() / right);
	}

	//
	// operator/
	//
	ATLAS_API Time operator/(Time left, i64 right)
	{
		return SetMicroseconds(left.AsMicroseconds() / right);
	}

	//
	// operator/=
	//
	ATLAS_API Time& operator/=(Time& left, f32 right)
	{
		return left = left / right;
	}

	//
	// operator/=
	//
	ATLAS_API Time& operator/=(Time& left, i64 right)
	{
		return left = left / right;
	}

	//
	// operator/
	//
	ATLAS_API f32 operator/(Time left, Time right)
	{
		return left.AsSeconds() / right.AsSeconds();
	}

	//
	// operator%
	//
	ATLAS_API Time operator%(Time left, Time right)
	{
		return SetMicroseconds(left.AsMicroseconds() % right.AsMicroseconds());
	}

	//
	// operator%=
	//
	ATLAS_API Time& operator%=(Time& left, Time right)
	{
		return left = left % right;
	}
} // Atlas