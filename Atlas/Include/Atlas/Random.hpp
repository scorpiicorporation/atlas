#pragma once

#include <Atlas/Types.hpp>

#include <random>

namespace Atlas
{
	class Random
	{
	public:
		//
		// Random - Ctor
		//
		Random() = default;

		//
		// Random - Ctor
		//
		Random(std::mt19937::result_type seed)
			: _engine(seed)
		{
		}

		//
		// SetSeed
		//
		void SetSeed(std::mt19937::result_type seed)
		{
			_engine.seed(seed);
		}

		//
		// GetInteger
		//
		i32 GetInteger(i32 min, i32 max)
		{
			std::uniform_int_distribution<i32> dist(min, max);
			return dist(_engine);
		}

		//
		// GetFloat
		//
		f32 GetFloat(f32 min, f32 max)
		{
			std::uniform_real_distribution<f32> dist(min, max);
			return dist(_engine);
		}

		//
		// GetBool
		//
		b8 GetBool()
		{
			return GetInteger(0, 1) == 1;
		}

	private:
		std::mt19937 _engine{ std::random_device{}() };
	};
} // Atlas