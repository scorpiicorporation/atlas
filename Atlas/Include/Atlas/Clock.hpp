#pragma once

#include <Atlas/Time.hpp>

namespace Atlas
{
	struct Clock
	{
		Time StartTime = Time::Now();

		inline Time GetElapsedTime() const { return Time::Now() - StartTime; }

		inline Time Restart()
		{
			auto now = Time::Now();
			auto elapsed = now - StartTime;
			StartTime = now;
			return elapsed;
		}
	}; // Clock
} // Atlas