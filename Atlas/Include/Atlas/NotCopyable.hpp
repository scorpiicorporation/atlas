#pragma once

#include <Atlas/Build.hpp>

namespace Atlas
{
	class ATLAS_API NotCopyable
	{
	protected:
		NotCopyable() { }
	private:
		NotCopyable(const NotCopyable&) = delete;
		NotCopyable& operator=(const NotCopyable&) = delete;

		NotCopyable(const NotCopyable&&) = delete;
		NotCopyable& operator=(const NotCopyable&&) = delete;

	}; // NotCopyable
} // Atlas