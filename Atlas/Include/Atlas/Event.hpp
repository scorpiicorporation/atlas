#pragma once

#include <Atlas/Input/Input.hpp>
#include <Atlas/Types.hpp>

namespace Atlas
{
	struct Event
	{
		struct SizeEvent
		{
			u32 width;
			u32 height;
		};

		struct MoveEvent
		{
			i32 x;
			i32 y;
		};

		struct KeyEvent
		{
			Input::Key code;
			bool alt;
			bool ctrl;
			bool shift;
			bool system;
			bool capsLock;
			bool numLock;
		};

		// TODO(deccer): TextEvents

		struct MouseMoveEvent
		{
			i32 x;
			i32 y;
		};

		struct MouseButtonEvent
		{
			Input::Mouse button;
			u8 clicks; // 1 for single-click, 2 for double-click, etc.
			i32 x;
			i32 y;
		};

		struct MouseWheelScrollEvent
		{
			i32 deltaX;
			i32 deltaY;
		};

		// TODO(deccer): Joystick events
		// TODO(deccer): Controller events
		// TODO(deccer): Drag and Drop Events
		// TODO(deccer): Clipboard Events

		struct ControllerButtonEvent
		{
			u32 index;
			Input::ControllerButton button;
		};

		struct ControllerConnectEvent
		{
			u32 index;
		};

		struct ControllerAxisEvent
		{
			u32 index;
			Input::ControllerAxis axis;
			f32 value;
		};

		enum EventType
		{
			Unknown, // A type that is not supported yet

			Closed,
			Resized,
			LostFocus,
			GainedFocus,
			Moved,

			KeyPressed,
			KeyReleased,

			MouseWheelScrolled,
			MouseButtonPressed,
			MouseButtonReleased,
			MouseMoved,
			MouseEntered,
			MouseLeft,

			ControllerButtonPressed,
			ControllerButtonReleased,

			ControllerConnected,
			ControllerDisconnected,
			ControllerRemapped,

			ControllerAxisMoved,

			// TODO(deccer): Implement all the event types possible

			Count // Keep last - total number of event types
		};

		EventType type;

		union
		{
			SizeEvent size;
			MoveEvent move;
			KeyEvent key;
			MouseMoveEvent mouseMove;
			MouseButtonEvent mouseButton;
			MouseWheelScrollEvent mouseWheelScroll;

			// TODO(deccer): Joystick event types
			ControllerConnectEvent controller;
			ControllerButtonEvent controllerButton;
			ControllerAxisEvent controllerAxis;
		};
	}; // Event
} // Atlas