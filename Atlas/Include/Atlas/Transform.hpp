#pragma once

#include <Atlas/Types.hpp>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace Atlas
{
	struct Transform
	{
		glm::vec3 Position = glm::vec3(0, 0, 0);
		glm::quat Orientation = glm::quat();
		glm::vec3 Scale = glm::vec3(1, 1, 1);
	};

	// World = Parent * Local
	ATLAS_API Transform operator*(const Transform& parentSpace, const Transform& localSpace);
	ATLAS_API Transform& operator*=(Transform& parentSpace, const Transform& localSpace);
	// Local = World / Parent
	ATLAS_API Transform operator/(const Transform& worldSpace, const Transform& parentSpace);
	ATLAS_API Transform& operator/=(Transform& worldSpace, const Transform& parentSpace);

	ATLAS_API Transform Inverse(const Transform& t);
	ATLAS_API glm::mat4 ToMat4(const Transform& transform);
	ATLAS_API glm::mat4 MatrixLookAt(const glm::vec3& eye, const glm::vec3& center, const glm::vec3& up);
	ATLAS_API glm::quat QuaternionLookAt(const glm::vec3& eye, const glm::vec3& center, const glm::vec3& up);
} // Atlas