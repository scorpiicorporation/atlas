#pragma once

#include <SDL.h>
#include <glm/vec2.hpp>

#include <Atlas/Types.hpp>
#include <Atlas/Event.hpp>

namespace Atlas
{
	namespace Window
	{
		extern SDL_Window* g_Ptr;
		extern SDL_GLContext g_ContextPtr;
		extern bool g_Fullscreen;

		ATLAS_API SDL_Window* CreateWindow(i32 monitor);
		ATLAS_API SDL_Window* CreateWindow(i32 monitor, u32 width, u32 height);

		ATLAS_API void CleanUp();
		ATLAS_API void DestroyWindow();
		ATLAS_API void DestroyWindow(SDL_Window* window);

		ATLAS_API glm::vec2 GetWindowSize();
		ATLAS_API glm::vec2 GetFramebufferSize();
		ATLAS_API bool Initialize();

		ATLAS_API void MakeContextCurrent();
		ATLAS_API bool PollEvent(Atlas::Event& event);

		ATLAS_API void SetSwapInterval(i32 interval);
		ATLAS_API void SetTitle(const char* title);
		ATLAS_API void SwapBuffers();
	} // Window
} // Atlas