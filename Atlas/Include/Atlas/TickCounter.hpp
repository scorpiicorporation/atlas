#pragma once

#include <Atlas/Clock.hpp>
#include <Atlas/Time.hpp>

namespace Atlas
{
	class TickCounter
	{
	public:
		//
		// GetTickRate
		//
		inline f64 GetTickRate() const
		{
			return _tickRate;
		}

		//
		// Update
		//
		bool Update(Time period)
		{
			bool reset = false;
			if (_clock.GetElapsedTime() >= period)
			{
				_tickRate = _ticks * (1.0f / period.AsSeconds());
				_ticks = 0;
				reset = true;
				_clock.Restart();
			}
			_ticks++;
			return reset;
		}
	private:
		u32 _ticks = 0;
		f64 _tickRate = 0;
		Clock _clock;
	}; // TickCounter
} // Atlas