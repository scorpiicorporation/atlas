#pragma once

#if ATLAS_EXPORTS
    #define ATLAS_API __declspec(dllexport)
#else
    #define ATLAS_API __declspec(dllimport)
#endif

#ifndef GLEW_STATIC
#define GLEW_STATIC
#endif

#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG
#endif

//#define GLM_FORCE_CXX11
#define GLM_FORCE_INLINE

#define B3_USE_CLEW

namespace Atlas
{
	struct Clock;
	struct Color;
	struct Event;

	class NotCopyable;
	class Random;
	class TickCounter;
	class Time;
	struct Transform;
	
	class GBuffer;
	class Image;
	class RenderTarget;
	class Shader;
	class Texture;
}