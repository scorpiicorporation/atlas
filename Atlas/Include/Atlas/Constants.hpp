#pragma once

#include <Atlas/Types.hpp>

#include <limits>

namespace Atlas
{
	namespace Constants
	{
		GLOBAL const f32 Epsilon = std::numeric_limits<f32>::epsilon();

		namespace Meshes
		{
			GLOBAL i32 Sprite = 0;
			GLOBAL i32 Quad = 1;
		}

		namespace Materials
		{
			GLOBAL i32 Default = 0;
			GLOBAL i32 Player = 1;
			GLOBAL i32 Stone = 2;
			GLOBAL i32 Terrain = 3;
		}

		namespace Textures
		{
			GLOBAL i32 Default_Diffuse = 0;
			GLOBAL i32 Default_Normal = 1;
			GLOBAL i32 Player_Diffuse = 2;
			GLOBAL i32 Player_Normal = 3;
			GLOBAL i32 Stone_Diffuse = 4;
			GLOBAL i32 Stone_Normal = 5;
			GLOBAL i32 Terrain_Diffuse = 6;
			GLOBAL i32 Terrain_Normal = 7;
		}

		namespace Shaders
		{
			GLOBAL i32 RenderPassFSQ = 0;
			GLOBAL i32 RenderPassObject = 1;
			GLOBAL i32 RenderPassLightAmbient = 2;
			GLOBAL i32 RenderPassLightDirectional = 3;
			GLOBAL i32 RenderPassLightPoint = 4;
			GLOBAL i32 RenderPassLightSpot = 5;
			GLOBAL i32 RenderPassFinal = 6;
		}
	};
} // Atlas