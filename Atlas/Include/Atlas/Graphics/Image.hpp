#pragma once

#include <Atlas/Types.hpp>

namespace Atlas
{
	namespace Graphics
	{
		enum class ImageFormat
		{
			None = 0,
			Greyscale = 1,
			GreyscaleAlpha = 2,
			RGB = 3,
			RGBA = 4,
		};

		class ATLAS_API Image
		{
		public:
			Image();
			Image(u32 width, u32 height, ImageFormat format, const u8* pixels = nullptr);
			Image(const Image& other);
			Image& operator=(const Image& other);

			~Image();

			bool LoadFromFile(const char* fileName);
			bool LoadFromMemory(u32 width, u32 height, ImageFormat format, const u8* pixels);

			inline u32 GetWidth() const { return _width; }
			inline u32 GetHeight() const { return _height; }
			inline ImageFormat GetFormat() const { return _format; }
			inline u8* GetPixels() const { return _pixels; }

			void FlipVertically();
		private:
			ImageFormat _format;
			u32 _width;
			u32 _height;
			u8* _pixels;
		}; // Image
	} // Graphics
} // Atlas