#pragma once

#include <Atlas/Build.hpp>
#include <Atlas/Types.hpp>

#include <SDL.h>
#include <GL/glew.h>

#include <cstdio>
#include <cstdlib>

//
// CheckOpenGLError
//
inline void CheckOpenGLError(const char* stmt, const char* fname, Atlas::i32 line)
{
	auto error = glGetError();
	if (error != GL_NO_ERROR)
	{
		char* errorString;
		switch (error)
		{
		case GL_NO_ERROR: errorString = "GL_NO_ERROR"; break;
		case GL_INVALID_ENUM: errorString = "GL_INVALID_ENUM"; break;
		case GL_INVALID_VALUE: errorString = "GL_INVALID_VALUE"; break;
		case GL_INVALID_OPERATION: errorString = "GL_INVALID_OPERATION"; break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: errorString = "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
		case GL_OUT_OF_MEMORY: errorString = "GL_OUT_OF_MEMORY"; break;
		case GL_STACK_UNDERFLOW: errorString = "GL_STACK_UNDERFLOW"; break; // Until OpenGL 4.2
		case GL_STACK_OVERFLOW: errorString = "GL_STACK_OVERFLOW"; break; // Until OpenGL 4.2
		default: errorString = "invalid error number"; break;
		}
		//auto errorString = glewGetErrorString(error);
		printf("OpenGL error %08x - %s, at %s:%i - for %s\n", error, errorString, fname, line, stmt);
		abort();
	}
}

#ifdef _DEBUG
#define GL_CHECK(stmt) do { \
stmt; \
CheckOpenGLError(#stmt, __FILE__, __LINE__); \
} while (0)
#else
#define GL_CHECK(stmt) stmt
#endif


//TODO(deccer): get that working properly across all source files
#ifndef GL_CHECK
#define GL_CHECK(stmt) stmt
#endif