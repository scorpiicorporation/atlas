#pragma once

#include <Atlas/Types.hpp>
#include <Atlas/NotCopyable.hpp>
#include <Atlas/Transform.hpp>
#include <Atlas/Color.hpp>

#include <Atlas/Graphics/OpenGL.hpp>

#include <iostream>
#include <string>
#include <fstream>
#include <map>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x3.hpp>
#include <glm/mat4x4.hpp>

namespace Atlas
{
	namespace Graphics
	{

		enum class ShaderType
		{
			VertexShader,
			FragmentShader
		};

		class ATLAS_API Shader : public NotCopyable
		{
		public:
			Shader();
			virtual ~Shader();

			bool AttachShaderFromFile(ShaderType shaderType, const std::string& fileName);
			bool AttachShaderFromMemory(ShaderType shaderType, const std::string& shaderSource);

			void Apply() const;
			void Unapply() const;

			bool IsApplied() const;

			bool Link();

			void BindAttributeLocation(GLuint location, const std::string& attributeName);
			GLint GetAttributeLocation(const std::string& attributeName);
			GLint GetUniformLocation(const std::string& uniformName);

			void SetUniform(const std::string& uniformName, f32 x);
			void SetUniform(const std::string& uniformName, f32  x, f32  y);
			void SetUniform(const std::string& uniformName, f32  x, f32  y, f32  z);
			void SetUniform(const std::string& uniformName, f32  x, f32  y, f32  z, f32  w);

			void SetUniform(const std::string& uniformName, i32 value);
			void SetUniform(const std::string& uniformName, bool value);

			void SetUniform(const std::string& uniformName, const glm::mat3x3& value);
			void SetUniform(const std::string& uniformName, const glm::mat4& value);
			void SetUniform(const std::string& uniformName, const glm::mat4x3& value);
			void SetUniform(const std::string& uniformName, const glm::vec2& value);
			void SetUniform(const std::string& uniformName, const glm::vec3& value);
			void SetUniform(const std::string& uniformName, const glm::vec4& value);
			void SetUniform(const std::string& uniformName, const glm::quat& value);
			void SetUniform(const std::string& uniformName, const Transform& value);
			void SetUniform(const std::string& uniformName, const Color& value);

			inline GLuint GetHandle() const { return _programHandle; }
			inline const std::string& GetErrorLog() const { return _errorLog; }

		private:
			GLuint _programHandle;
			bool _isLinked;
			std::string _errorLog;
			std::map<std::string, GLint> _uniformLocations;
			std::map<std::string, GLint> _attributeLocations;

			void IsBound() const;
		}; // Shader
	} // Graphics
} // Atlas