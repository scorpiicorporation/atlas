#pragma once

#include <Atlas/NotCopyable.hpp>
#include <Atlas/Graphics/Image.hpp>
#include <Atlas/Graphics/OpenGL.hpp>

namespace Atlas
{
	namespace Graphics
	{
		enum class TextureFilter : i32
		{
			Nearest = GL_NEAREST,
			Linear = GL_LINEAR
		};

		enum class TextureWrap : i32
		{
			Clamp = GL_CLAMP,
			Repeat = GL_REPEAT,
			ClampToEdge = GL_CLAMP_TO_EDGE,
			ClampToBorder = GL_CLAMP_TO_BORDER,
			MirroredRepeat = GL_MIRRORED_REPEAT
		};

		class ATLAS_API Texture : public NotCopyable
		{
		public:
			Texture();
			Texture(const Image& image, TextureFilter textureFilter = TextureFilter::Linear, TextureWrap textureWrap = TextureWrap::ClampToEdge);
			virtual ~Texture();

			bool LoadFromFile(const char* fileName, TextureFilter textureFilter = TextureFilter::Linear, TextureWrap textureWrap = TextureWrap::ClampToEdge);
			bool LoadFromImage(const Image& image, TextureFilter textureFilter = TextureFilter::Linear, TextureWrap textureWrap = TextureWrap::ClampToEdge);

			GLOBAL void Bind(const Texture* texture, Atlas::u32 position);

			inline Atlas::u32 GetNativeHandle() const { return _nativeHandle; }
			inline Atlas::i32 GetWidth() const { return _width; }
			inline Atlas::i32 GetHeight() const { return _height; }

			inline void SetHeight(Atlas::i32 height)
			{
				_height = height;
			}

			inline void SetWidth(Atlas::i32 width)
			{
				_width = width;
			}
		private:
			Atlas::u32 _nativeHandle;
			Atlas::i32 _width;
			Atlas::i32 _height;
		}; // Texture
	} // Graphics
} // Atlas