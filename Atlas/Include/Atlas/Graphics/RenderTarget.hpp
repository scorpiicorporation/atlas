#pragma once

#include <Atlas/Types.hpp>
#include <Atlas/Graphics/OpenGL.hpp>
#include <Atlas/Graphics/Texture.hpp>

namespace Atlas
{
	namespace Graphics
	{
		class ATLAS_API RenderTarget
		{
		public:
			enum TextureType
			{
				Color = 1,
				Depth = 2,
				ColorAndDepth = Color | Depth,
				Lighting = 4 | Color,
			};

			RenderTarget();
			virtual ~RenderTarget();

			bool Create(i32 width, i32 height, TextureType type = ColorAndDepth, TextureFilter minMagFilter = TextureFilter::Linear, TextureWrap wrapMode = TextureWrap::ClampToEdge);

			GLOBAL void Bind(const RenderTarget* renderTarget);

			i32 GetWidth() const;
			i32 GetHeight() const;

			TextureType GetType() const;

			GLuint GetNativeHandle() const;

			Texture colorTexture;// TODO(deccer): add getter/setter
			Texture depthTexture;// TODO(deccer): add getter/setter

		private:
			i32 _width;
			i32 _height;

			TextureType _type;
			GLuint _fbo; // framebuffer
		}; // RenderTarget
	} // Graphics
} // Atlas