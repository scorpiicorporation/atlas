#pragma once

#include <Atlas/Types.hpp>
#include <Atlas/Graphics/OpenGL.hpp>
#include <Atlas/Graphics/Texture.hpp>

namespace Atlas
{
	namespace Graphics
	{
		class ATLAS_API GBuffer
		{
		public:
			GBuffer();
			virtual ~GBuffer();

			bool Create(u32 width, u32 height);

			GLOBAL void Bind(const GBuffer* buffer);

			u32 GetWidth() const;
			u32 GetHeight() const;

			GLuint GetNativeHandle() const;

			// TODO(deccer): Specific Method(s) for texture retrieval

			Texture Diffuse;
			Texture Specular;
			Texture Normal;
			// Texture Emission;
			Texture Depth;

		private:
			u32 _width;
			u32 _height;

			GLuint _nativeHandle;
		}; // GBuffer
	} // Graphics
} // Atlas