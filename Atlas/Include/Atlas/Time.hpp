#pragma once

#include <Atlas/Types.hpp>

#include <chrono>
#include <thread>

namespace Atlas
{
	class ATLAS_API Time
	{
	public:
		Time() = default;
		explicit Time(i64 microseconds);

		GLOBAL const Time Zero;

		f32 AsSeconds() const;
		i32 AsMilliseconds() const;
		i64 AsMicroseconds() const;

		GLOBAL Time Now();
		GLOBAL void Sleep(Time time);
	private:
		std::chrono::microseconds _microseconds;
	};

	Time SetSeconds(f32 amount)
	{
		return Time(static_cast<i64>(amount * 1000000));
	}

	Time SetMilliseconds(i32 amount)
	{
		return Time(static_cast<i64>(amount * 1000));
	}

	Time SetMicroseconds(i64 amount)
	{
		return Time(amount);
	}

	ATLAS_API bool operator==(Time left, Time right);
	ATLAS_API bool operator!=(Time left, Time right);

	ATLAS_API bool operator<(Time left, Time right);
	ATLAS_API bool operator>(Time left, Time right);

	ATLAS_API bool operator<=(Time left, Time right);
	ATLAS_API bool operator>=(Time left, Time right);

	ATLAS_API Time operator-(Time right);

	ATLAS_API Time operator+(Time left, Time right);
	ATLAS_API Time operator-(Time left, Time right);

	ATLAS_API Time& operator+=(Time& left, Time right);
	ATLAS_API Time& operator-=(Time& left, Time right);

	ATLAS_API Time operator*(Time left, f32 right);
	ATLAS_API Time operator*(Time left, i64 right);
	ATLAS_API Time operator*(f32 left, Time right);
	ATLAS_API Time operator*(i64 left, Time right);

	ATLAS_API Time& operator*=(Time& left, f32 right);
	ATLAS_API Time& operator*=(Time& left, i64 right);

	ATLAS_API Time operator/(Time left, f32 right);
	ATLAS_API Time operator/(Time left, i64 right);

	ATLAS_API Time& operator/=(Time& left, f32 right);
	ATLAS_API Time& operator/=(Time& left, i64 right);

	ATLAS_API f32 operator/(Time left, Time right);

	ATLAS_API Time operator%(Time left, Time right);
	ATLAS_API Time& operator%=(Time& left, Time right);
} // Atlas