#pragma once

#include <Atlas/Types.hpp>

namespace Metaverse
{
	namespace Game
	{
		void CleanUp();
		bool Initialize();
		void Run();
	} // Game
} // Metaverse