#include <Metaverse/Game.hpp>

int main(int argc, char* argv[])
{
	if (Metaverse::Game::Initialize())
	{
		Metaverse::Game::Run();
		Metaverse::Game::CleanUp();
	}
	return 0;
}
