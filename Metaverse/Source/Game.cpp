#include <Atlas/Event.hpp>
#include <Atlas/Time.hpp>
#include <Atlas/TickCounter.hpp>
#include <Atlas/Transform.hpp>
#include <Atlas/Window.hpp>
#include <Atlas/Input/Input.hpp>
#include <Metaverse/Game.hpp>

#include <Atlas/Graphics/OpenGL.hpp>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/matrix4x4.h>
#include <assimp/postprocess.h>

#include <chrono>
#include <iostream>

namespace Metaverse
{
	namespace
	{
		Atlas::i32 windowWidth = 1920;
		Atlas::i32 windowHeight = 1080;

		GLOBAL const Atlas::Time g_TimeStep = Atlas::SetSeconds(1.0f / 60.0f);
	}

	namespace Game
	{
		GLOBAL bool g_IsRunning = true;
		GLOBAL bool g_IsFullscreen = false;

		GLOBAL const aiScene* g_Scene = nullptr;
		GLOBAL Atlas::u32 g_SceneList = 0;

		//
		// Forward Declarations
		//
		INTERNAL void Update(Atlas::Time deltaTime);

		void CleanUp()
		{
			Atlas::Input::CleanUp();
			Atlas::Window::CleanUp();
		}

		INTERNAL void HandleInput(const Atlas::Time deltaTime)
		{
			Atlas::Event event;
			while (Atlas::Window::PollEvent(event))
			{
				switch (event.type)
				{
				case Atlas::Event::Closed:
				{
					Atlas::Window::DestroyWindow();
					exit(EXIT_SUCCESS);
					break;
				}
				case Atlas::Event::Resized:
				{
					glViewport(0, 0, event.size.width, event.size.height);
					break;
				}
				case Atlas::Event::ControllerConnected:
				{
					printf("Controller %d added\n", event.controller.index);
					break;
				}
				case Atlas::Event::ControllerDisconnected:
				{
					printf("Controller %d removed\n", event.controller.index);
					break;
				}
				case Atlas::Event::KeyPressed:
				{
					if (event.key.code == Atlas::Input::Key::Unknown)
						break;

					//logPrintf(g_logger, "Event::KeyPressed == %d\n", event.key.code);
					switch (event.key.code)
					{
					case Atlas::Input::Key::Escape:
					{
						Atlas::Window::DestroyWindow();
						exit(EXIT_SUCCESS);
						break;
					}
					default:
						break;
					}
					break;
				}
				default:
					break;
				}
			}
		}

		bool Initialize()
		{
			if (!Atlas::Window::Initialize())
			{
				std::cout << "Game: Unable to create a window\n";
				return false;
			}
			Atlas::Input::Initialize();
			return true;
		}

		// =============================================================================================
		/* ---------------------------------------------------------------------------- */
		void recursive_render(const struct aiScene *sc, const struct aiNode* nd)
		{
			unsigned int i;
			unsigned int n = 0, t;
			auto m = nd->mTransformation;

			/* update transform */
			aiTransposeMatrix4(&m);
			glPushMatrix();
			glMultMatrixf((float*)&m);

			/* draw all meshes assigned to this node */
			for (; n < sc->mNumMeshes; ++n) {
				const struct aiMesh* mesh = sc->mMeshes[n];

				//apply_material(sc->mMaterials[mesh->mMaterialIndex]);

				if (mesh->mNormals == NULL) {
					glDisable(GL_LIGHTING);
				}
				else {
					glEnable(GL_LIGHTING);
				}

				for (t = 0; t < mesh->mNumFaces; ++t) {
					const struct aiFace* face = &mesh->mFaces[t];
					GLenum face_mode;

					switch (face->mNumIndices) {
					case 1: face_mode = GL_POINTS; break;
					case 2: face_mode = GL_LINES; break;
					case 3: face_mode = GL_TRIANGLES; break;
					default: face_mode = GL_POLYGON; break;
					}

					glBegin(face_mode);

					for (i = 0; i < face->mNumIndices; i++) {
						int index = face->mIndices[i];
						if (mesh->mColors[0] != NULL)
							glColor4fv((GLfloat*)&mesh->mColors[0][index]);
						if (mesh->mNormals != NULL)
							glNormal3fv(&mesh->mNormals[index].x);
						glVertex3fv(&mesh->mVertices[index].x);
					}

					glEnd();
				}

			}

			/* draw all children */
			for (n = 0; n < nd->mNumChildren; ++n) {
				recursive_render(sc, nd->mChildren[n]);
			}

			glPopMatrix();
		}
		// =============================================================================================

		GLOBAL Atlas::f32 test = 0;

		void Render()
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			
			if (g_SceneList == 0) {
				g_SceneList = glGenLists(1);
				auto error = glGetError();
				char* errorString;
				switch (error)
				{
				case GL_INVALID_ENUM: errorString = "GL_INVALID_ENUM"; break;
				case GL_INVALID_VALUE: errorString = "GL_INVALID_VALUE"; break;
				case GL_INVALID_OPERATION: errorString = "GL_INVALID_OPERATION"; break;
				case GL_INVALID_FRAMEBUFFER_OPERATION: errorString = "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
				case GL_OUT_OF_MEMORY: errorString = "GL_OUT_OF_MEMORY"; break;
				case GL_STACK_UNDERFLOW: errorString = "GL_STACK_UNDERFLOW"; break; // Until OpenGL 4.2
				case GL_STACK_OVERFLOW: errorString = "GL_STACK_OVERFLOW"; break; // Until OpenGL 4.2
				default: errorString = "invalid error number"; break;
				}
				std::cout << errorString << "\n.";

				glNewList(g_SceneList, GL_COMPILE);

				recursive_render(g_Scene, g_Scene->mRootNode);
				glEndList();
			}
			
			glShadeModel(GL_FLAT);

			glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
			glLoadIdentity();									// Reset The Projection Matrix
																// Calculate The Aspect Ratio Of The Window
			gluPerspective(45.0f, (GLfloat)16 / (GLfloat)9, 0.1f, 8192.0f);

			glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
			glLoadIdentity();

			glTranslatef(0.0f, 0.0f, -10.0f);
			glRotatef(test, 0.0f, 1.0f, 0.0f);
			//glRotatef(test, 0.0f, 0.0f, 0.001f);
			test += 0.1f;
			if (test >= 360.0f)
			{
				test = 0.0f;
			}

			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);

			// Create light components
			GLfloat ambientLight[] = { 0.0f, 0.1f, 0.2f, 1.0f };
			GLfloat diffuseLight[] = { 0.2f, 0.6f, 1.0, 1.0f };
			GLfloat specularLight[] = { 1.0f, 0.0f, 0.0f, 1.0f };
			GLfloat position[] = { sin(test * 0.1f) * 50.0f, cos(test * 0.1f) * 34.0f, -cos(test * 0.1f) * 50.0f, 1.0f };

			glScalef(0.1f, 0.1f, 0.1f);
			glTranslatef(position[0], position[1], position[2]);
			glCallList(g_SceneList);

			// Assign created components to GL_LIGHT0
			glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
			glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
			glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
			glLightfv(GL_LIGHT0, GL_POSITION, position);

			glLoadIdentity();
			glScalef(1.0f, 1.0f, 1.0f);
			glTranslatef(0.0f, 0.0f, -10.0f);
			glRotatef(test, 0.0f, 1.0f, 0.0f);
			glCallList(g_SceneList);
			
		}

		void Run()
		{
			auto modelFileName = "Data/Models/firstship.obj";
			g_Scene = aiImportFile(modelFileName, aiProcessPreset_TargetRealtime_MaxQuality);
			if (g_Scene == nullptr)
			{
				std::cout << "Game: Unable to load model \"" << modelFileName << "\".\n";
			}

			Atlas::TickCounter tickCounter;
			Atlas::Time accumulator;
			Atlas::Time previousTime = Atlas::Time::Now();

			while (g_IsRunning)
			{
				Atlas::Time currentTime = Atlas::Time::Now();
				Atlas::Time deltaTime = currentTime - previousTime;
				previousTime = currentTime;
				accumulator += deltaTime;
				if (accumulator > Atlas::SetMilliseconds(1200))
				{
					accumulator = Atlas::SetMilliseconds(1200);
				}
				HandleInput(deltaTime);
				while (accumulator >= g_TimeStep)
				{
					accumulator -= g_TimeStep;
					HandleInput(deltaTime);
					Update(deltaTime);
				}
				Render();
				if (tickCounter.Update(Atlas::SetMilliseconds(500)))
				{
					std::cout << "Metaverse " << 1000.0f / tickCounter.GetTickRate() << "ms " << tickCounter.GetTickRate() << " fps\n";
				}
				Atlas::Window::SwapBuffers();
			}

			aiReleaseImport(g_Scene);
		}

		INTERNAL void Update(Atlas::Time deltaTime)
		{

		}
	} // Game
} // Metaverse